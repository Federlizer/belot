// Package gamecommands is a collection of commands that are specific to a game
package gamecommands

import (
	"errors"
	"fmt"
	"log"
	"reflect"

	"gitlab.com/Federlizer/belot/game/models"
	"gitlab.com/Federlizer/belot/protocol"
)

// SelectTrumpCommand is a command that signifies a trump has been selected for this game
const SelectTrumpCommand = "select-trump"

// SelectedTrumpPayload is the expected payload from a SelectTrumpCommand
type SelectedTrumpPayload struct {
	Trump models.Trump
}

// ValidateSelectedTrumpPayload validates the payload comming in a command. Will panic if the command
// name doesn't equal `SelectTrumpCommand`
func ValidateSelectedTrumpPayload(command *protocol.Command) error {
	if command.Command != SelectTrumpCommand {
		panic(fmt.Sprintf("not a %v command", SelectTrumpCommand))
	}

	trumpInterface, ok := command.Payload["trump"]
	if !ok {
		return errors.New("'trump' key must be present for this command")
	}

	trumpType := reflect.TypeOf(trumpInterface)
	if trumpType.Kind() != reflect.String {
		return errors.New("'trump' key must be of type string")
	}

	// Validate the value as well
	trumpValue := reflect.ValueOf(trumpInterface).String()

	log.Printf("validate: trumpValue is %v", trumpValue)

	trump, err := models.StringToTrump(trumpValue)
	if err != nil {
		return err
	}

	log.Printf("validate: Received trump back %v", trump)

	return nil
}

// ParseSelectedTrumpPayload attempts to parse the payload of a selected trump command. It will not check the payload passed,
// so make sure to run the ValidateSelectedTrumpPayload function first.
func ParseSelectedTrumpPayload(command *protocol.Command) *SelectedTrumpPayload {
	trumpInterface := command.Payload["trump"]
	trumpValue := reflect.ValueOf(trumpInterface).String()

	log.Printf("parse: trumpValue is %v", trumpValue)

	trump, err := models.StringToTrump(trumpValue)

	log.Printf("parse: receoved trump back %v", trump)

	if err != nil {
		panic(err)
	}

	return &SelectedTrumpPayload{
		Trump: trump,
	}
}

// ValidateAndParseSelectedTrumpPayload first validates the payload and then parses it, returning the payload
// that was sent in the command. If there are any errors,  SelectedTrumpPayload is going to be nil.
func ValidateAndParseSelectedTrumpPayload(command *protocol.Command) (*SelectedTrumpPayload, error) {
	err := ValidateSelectedTrumpPayload(command)
	if err != nil {
		return nil, err
	}

	payload := ParseSelectedTrumpPayload(command)
	return payload, err
}
