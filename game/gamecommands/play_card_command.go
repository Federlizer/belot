// Package gamecommands is a collection of commands that are specific to a game
package gamecommands

import (
	"errors"
	"fmt"
	"reflect"

	"gitlab.com/Federlizer/belot/game/models"
	"gitlab.com/Federlizer/belot/protocol"
)

// PlayCardCommand is a command that signifies a player has played a card
const PlayCardCommand = "play-card"

// PlayCardPayload is the expected payload from a PlayCardCommand
type PlayCardPayload struct {
	Card models.Card
}

// ValidatePlayCardPayload validates the payload comming in a command. Will panic
// if the command name doesn't equal `PlayCardCommand`
func ValidatePlayCardPayload(command *protocol.Command) error {
	if command.Command != PlayCardCommand {
		panic(fmt.Sprintf("not a %v command", PlayCardCommand))
	}

	// Validate suit
	suitInterface, ok := command.Payload["suit"]
	if !ok {
		return errors.New("'suit' key must be present for this command")
	}

	suitType := reflect.TypeOf(suitInterface)
	if suitType.Kind() != reflect.String {
		return errors.New("'suit' key must be of type string")
	}

	suitValue := reflect.ValueOf(suitInterface).String()
	_, err := models.StringToSuit(suitValue)
	if err != nil {
		return err
	}

	// Validate rank
	rankInterface, ok := command.Payload["rank"]
	if !ok {
		return errors.New("'rank' key must be present for this command")
	}

	rankType := reflect.TypeOf(rankInterface)
	if rankType.Kind() != reflect.String {
		return errors.New("'rank' key must be of type string")
	}

	rankValue := reflect.ValueOf(rankInterface).String()
	_, err = models.StringToRank(rankValue)
	if err != nil {
		return err
	}

	return nil
}

// ParsePlayCardPayload attempts to parse the payload of a PlayCardCommand. It will not check the payload passed,
// so make sure to run the ValidatePlayCardPayload function first.
func ParsePlayCardPayload(command *protocol.Command) *PlayCardPayload {
	suitInterface := command.Payload["suit"]
	suitValue := reflect.ValueOf(suitInterface).String()
	suit, err := models.StringToSuit(suitValue)
	if err != nil {
		panic(err)
	}

	rankInterface := command.Payload["rank"]
	rankValue := reflect.ValueOf(rankInterface).String()
	rank, err := models.StringToRank(rankValue)
	if err != nil {
		panic(err)
	}

	return &PlayCardPayload{
		Card: *models.NewCard(rank, suit),
	}
}

// ValidateAndParsePlayCardPayload first validates the payload and then parses it, returning the payload
// that was sent in the command. If there are any errors, PlayCardPayload is going to be nil.
func ValidateAndParsePlayCardPayload(command *protocol.Command) (*PlayCardPayload, error) {
	err := ValidatePlayCardPayload(command)
	if err != nil {
		return nil, err
	}

	payload := ParsePlayCardPayload(command)
	return payload, err
}
