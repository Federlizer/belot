// Package gamecommands is a collection of commands that are specific to a game
package gamecommands

// ReadyCommand is a command that signifies a player is ready to play the game
const ReadyCommand = "ready"
