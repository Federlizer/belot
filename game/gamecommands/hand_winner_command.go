// Package gamecommands is a collection of commands that are specific to a game
package gamecommands

import (
	"errors"
	"fmt"
	"reflect"

	"gitlab.com/Federlizer/belot/protocol"
)

// HandWinnerCommand is a command that tells the game server which player has won the hand
const HandWinnerCommand = "hand-winner"

// HandWinnerPayload is the expected payload from a HandWinnerCommand
type HandWinnerPayload struct {
	Username string
}

// ValidateHandWinnerPayload validates the payload comming in a command. Will panic
// if the command name doesn't equal `HandWinnerCommand`
func ValidateHandWinnerPayload(command *protocol.Command) error {
	if command.Command != HandWinnerCommand {
		panic(fmt.Sprintf("not a %v command", HandWinnerCommand))
	}

	// Validate username
	usernameInterface, ok := command.Payload["username"]
	if !ok {
		return errors.New("'username' key must be present for this command")
	}

	usernameType := reflect.TypeOf(usernameInterface)
	if usernameType.Kind() != reflect.String {
		return errors.New("'username' key must be of type string")
	}

	return nil
}

// ParseHandWinnerPayload attempts to parse the payload of a HandWinnerCommand. It will not check the
// passed payload, so make sure to run the ValidateHandWinnerPayload function first.
func ParseHandWinnerPayload(command *protocol.Command) *HandWinnerPayload {
	usernameInterface := command.Payload["username"]
	usernameValue := reflect.ValueOf(usernameInterface)
	username := usernameValue.String()

	return &HandWinnerPayload{
		Username: username,
	}
}

// ValidateAndParseHandWinnerPayload first validates the payload and then parses it, returning the payload
// that was sent in the command. If there are any errors, HandWinnerPayload is going to be nil.
func ValidateAndParseHandWinnerPayload(command *protocol.Command) (*HandWinnerPayload, error) {
	err := ValidateHandWinnerPayload(command)
	if err != nil {
		return nil, err
	}

	payload := ParseHandWinnerPayload(command)
	return payload, err
}
