// Package game contains the game logic
package game

import "gitlab.com/Federlizer/belot/game/models"

// Gameboard is a struct that describes the current gameboard state
type Gameboard struct {
	// key is the username
	Hands      map[string]*GameboardHand `json:"hands"`
	BoardCards map[string]*models.Card   `json:"boardCards"`
}

// GameboardHand is a struct that describes the hands of players.
// Cards can be an empty array if the player doesn't hold the hand
type GameboardHand struct {
	Hand       []*models.Card `json:"hand"`
	HandLength int            `json:"handLength"`
}
