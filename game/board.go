// Package game contains the game logic
package game

import "gitlab.com/Federlizer/belot/game/models"

// BoardCards represents the current played cards by each player in the game
type BoardCards map[*Player]*models.Card

// cards method returns all cards currently set on the board
func (b BoardCards) cards() []*models.Card {
	var allCards []*models.Card
	for _, card := range b {
		allCards = append(allCards, card)
	}

	return allCards
}

// clean function cleans the board from all cards and sets it up for a new hand
func (b BoardCards) clear() {
	for p := range b {
		delete(b, p)
	}
}
