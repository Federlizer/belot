// Package gameevents is a package containing in-game events
package gameevents

// CardsDealtEvent is an event letting players know that cards have been dealt out
const CardsDealtEvent = "cards-dealt"
