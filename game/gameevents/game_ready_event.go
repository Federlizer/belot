// Package gameevents is a package containing in-game events
package gameevents

// GameReadyEvent is an event letting players know that the game has been started. For each player, the information
// given is from their perspective (playerSouth is always going to be the playing Player).
const GameReadyEvent = "game-ready"

/*
eventData:
    event: game-ready
    value:
        playerSouth:
            username: Federlizer
        playerEast:
            username: Sh0lta
        playerWest:
            username: DAnonimousT
        playerNorth:
            username: PineappleBGR
*/
