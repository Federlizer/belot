// Package gameevents is a package containing in-game events
package gameevents

// ActionRequestEvent is an event letting players know that someone should take action
const ActionRequestEvent = "action-request"

/*
eventData:
    event: action-request
    value:
        player: Federlizer
        actionRequested: select-trump
*/
