// Package gameevents is a package containing in-game events
package gameevents

// GameOverEvent is an event letting players know that the game is now over.
const GameOverEvent = "game-over"

/*
eventData:
*/
