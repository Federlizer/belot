// Package gameevents is a package containing in-game events
package gameevents

// UpdateGameboardEvent is an event letting clients know that they should update the gameboard with the new data
const UpdateGameboardEvent = "update-gameboard"

/*
eventData:
    event: update-gameboard
    value:
        Federlizer:
            hand: [ ... ],
            handLength: 8,
        Sh0lta:
            hand: [],
            handLength: 8,
        DAnonimousT:
            hand: [],
            handLength: 8,
        PineappleBGR:
            hand: [],
            handLength: 8,
*/
