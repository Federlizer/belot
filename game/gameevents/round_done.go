// Package gameevents is a package containing in-game events
package gameevents

// RoundDoneEvent is an event letting players know that the round has been completed
const RoundDoneEvent = "round-done"

/*
eventData:
  teamOneCards:
    - card1
    - card2
    - card3
    ...
  teamTwoCards:
    - card1
    - card2
    - card3
    ...
*/
