package game_test

import (
	"testing"

	"gitlab.com/Federlizer/belot/game/models"
)

type isHigherThanTest struct {
	t        models.Trump
	other    models.Trump
	expected bool
}

var isHigherThanTests = []isHigherThanTest{
	{models.AllTrump, models.AllTrump, false},
	{models.AllTrump, models.NoTrump, true},
	{models.AllTrump, models.SpadesTrump, true},
	{models.AllTrump, models.HeartsTrump, true},
	{models.AllTrump, models.DiamondsTrump, true},
	{models.AllTrump, models.ClubsTrump, true},

	{models.NoTrump, models.AllTrump, false},
	{models.NoTrump, models.NoTrump, false},
	{models.NoTrump, models.SpadesTrump, true},
	{models.NoTrump, models.HeartsTrump, true},
	{models.NoTrump, models.DiamondsTrump, true},
	{models.NoTrump, models.ClubsTrump, true},

	{models.SpadesTrump, models.AllTrump, false},
	{models.SpadesTrump, models.NoTrump, false},
	{models.SpadesTrump, models.SpadesTrump, false},
	{models.SpadesTrump, models.HeartsTrump, true},
	{models.SpadesTrump, models.DiamondsTrump, true},
	{models.SpadesTrump, models.ClubsTrump, true},

	{models.HeartsTrump, models.AllTrump, false},
	{models.HeartsTrump, models.NoTrump, false},
	{models.HeartsTrump, models.SpadesTrump, false},
	{models.HeartsTrump, models.HeartsTrump, false},
	{models.HeartsTrump, models.DiamondsTrump, true},
	{models.HeartsTrump, models.ClubsTrump, true},

	{models.DiamondsTrump, models.AllTrump, false},
	{models.DiamondsTrump, models.NoTrump, false},
	{models.DiamondsTrump, models.SpadesTrump, false},
	{models.DiamondsTrump, models.HeartsTrump, false},
	{models.DiamondsTrump, models.DiamondsTrump, false},
	{models.DiamondsTrump, models.ClubsTrump, true},

	{models.ClubsTrump, models.AllTrump, false},
	{models.ClubsTrump, models.NoTrump, false},
	{models.ClubsTrump, models.SpadesTrump, false},
	{models.ClubsTrump, models.HeartsTrump, false},
	{models.ClubsTrump, models.DiamondsTrump, false},
	{models.ClubsTrump, models.ClubsTrump, false},
}

func TestIsTrumpHigherThanLower(t *testing.T) {
	for _, test := range isHigherThanTests {
		output := test.t.IsHigherThan(test.other)
		if output != test.expected {
			t.Errorf("Output %t not equal to %t for (%v, %v)", output, test.expected, test.t, test.other)
		}
	}
}
