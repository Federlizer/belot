// Package models_test tests the game models
package game_test

import (
	"testing"

	"gitlab.com/Federlizer/belot/game/models"
)

type stringToSuitTest struct {
	suitString    string
	errorExpected bool
	suitExpected  models.Suit
}

var stringToSuitTests = []stringToSuitTest{
	{"hearts", false, models.Hearts},
	{"spades", false, models.Spades},
	{"clubs", false, models.Clubs},
	{"diamonds", false, models.Diamonds},
	{"Diamonds", true, ""},
	{"something", true, ""},
}

func TestStringToSuit(t *testing.T) {
	for _, test := range stringToSuitTests {
		output, err := models.StringToSuit(test.suitString)

		if test.errorExpected && err == nil {
			t.Errorf("Expected an error but received err == nil instead. Error: %v, Output: %v", err, output)
		}

		if !test.errorExpected && err != nil {
			t.Errorf("Didn't expect an error but received an error. Error: %v, Output: %v", err, output)
		}

		if test.suitExpected != output {
			t.Errorf("Didn't receive expected output. Error: %v, Output: %v", err, output)
		}
	}
}
