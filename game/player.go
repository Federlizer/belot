// Package game contains the game logic
package game

import (
	"log"

	"gitlab.com/Federlizer/belot/game/gamecommands"
	"gitlab.com/Federlizer/belot/game/models"
	"gitlab.com/Federlizer/belot/protocol"
)

// Player struct represents a registered player in the hub/game
type Player struct {
	username string
	emit     chan<- *protocol.Event
	hand     []*models.Card
}

// NewPlayer creates a new player
func NewPlayer(username string, emitChan chan<- *protocol.Event) *Player {
	return &Player{
		username: username,
		emit:     emitChan,
		hand:     make([]*models.Card, 0),
	}
}

// TakeAwayCards takes away all cards in the hand of this player and returns them to
// the caller
func (p *Player) TakeAwayCards() []*models.Card {
	cards := p.hand
	p.hand = make([]*models.Card, 0)

	return cards
}

// AddCard adds a card to a player's hand
func (p *Player) AddCard(card *models.Card) {
	log.Printf("Giving card %v to player %v", card, p)
	p.hand = append(p.hand, card)
}

// GiveCards adds multiple cards to a player's hand
func (p *Player) GiveCards(cards []*models.Card) {
	log.Printf("Giving cards %v to player %v", cards, p)
	p.hand = append(p.hand, cards...)
}

// PlayCard asks a player to play a card. NOTE: Will panic if there are no cards in hand for this player
func (p *Player) PlayCard(actionCh <-chan *PlayerCommand) *models.Card {
	if len(p.hand) == 0 {
		panic("Not enough cards in hand")
	}

	log.Printf("Awaiting player %v to play a card", p.username)

	for {
		playerCmd := <-actionCh
		log.Printf("Received command while waiting for a card to be played: %v", playerCmd)

		if playerCmd.Player != p {
			log.Printf("%v is not current player, skipping", playerCmd.Player.username)
			playerCmd.Player.emit <- protocol.NewErrorEvent("Not your turn")
			continue
		}

		// Is it a play-card command?
		if playerCmd.Command.Command != gamecommands.PlayCardCommand {
			playerCmd.Player.emit <- protocol.NewErrorEvent("You need to play a card")
			continue
		}

		// Validate and parse payload
		payload, err := gamecommands.ValidateAndParsePlayCardPayload(playerCmd.Command)
		if err != nil {
			playerCmd.Player.emit <- protocol.NewErrorEvent(err.Error())
			continue
		}

		// Does the card exist in this player's hand?
		// NOTE: Remember that in this case the card that we've created has a different address
		// (since it wasn't created from the player's hand), so we need to find the exact match (suit + rank)

		cardIndex := -1
		for i, card := range p.hand {
			if card.Equals(&payload.Card) {
				cardIndex = i
			}
		}

		if cardIndex == -1 {
			// Card doesn't exist in this player's hand
			playerCmd.Player.emit <- protocol.NewErrorEvent("This card doesn't exist in your hand")
			continue
		}

		// Card exists, proceed to remove it from the hand
		card := p.hand[cardIndex]
		p.hand = append(p.hand[:cardIndex], p.hand[cardIndex+1:]...)

		return card
	}
}

// DecideHandWinner asks a player to decide on a hand winner.
func (p *Player) DecideHandWinner(actionCh <-chan *PlayerCommand) string {
	log.Printf("Awaiting player %v to decide on a hand winner", p.username)

	for {
		playerCmd := <-actionCh

		if playerCmd.Player != p {
			// Only dealer is allowed to decide this
			playerCmd.Player.emit <- protocol.NewErrorEvent("Only the dealer can decide who wins the hand")
			continue
		}

		if playerCmd.Command.Command != gamecommands.HandWinnerCommand {
			// Only hand winner commands
			playerCmd.Player.emit <- protocol.NewErrorEvent("You need to select a hand winner")
			continue
		}

		payload, err := gamecommands.ValidateAndParseHandWinnerPayload(playerCmd.Command)
		if err != nil {
			playerCmd.Player.emit <- protocol.NewErrorEvent(err.Error())
			continue
		}

		return payload.Username
	}
}
