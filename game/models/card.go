// Package models contains the models used through the game logic and it's supporters
package models

import (
	"fmt"
	"log"
)

// Suit is the suit
type Suit string

// Rank is the rank
type Rank string

// These are the suits
const (
	Spades   Suit = "spades"
	Clubs    Suit = "clubs"
	Diamonds Suit = "diamonds"
	Hearts   Suit = "hearts"
)

// These are the ranks
const (
	Ace   Rank = "A"
	Two   Rank = "2"
	Three Rank = "3"
	Four  Rank = "4"
	Five  Rank = "5"
	Six   Rank = "6"
	Seven Rank = "7"
	Eight Rank = "8"
	Nine  Rank = "9"
	Ten   Rank = "10"
	Jack  Rank = "J"
	Queen Rank = "Q"
	King  Rank = "K"
)

// Suits is an array with all possible suits
var Suits = [4]Suit{Spades, Clubs, Diamonds, Hearts}

// Ranks is an array with all possible ranks
var Ranks = [13]Rank{
	Ace, Two, Three, Four,
	Five, Six, Seven, Eight,
	Nine, Ten, Jack, Queen, King,
}

// StringToSuit converts a string to a suit or returns an error if invalid
func StringToSuit(suit string) (Suit, error) {
	log.Printf("Going to try to convert %v into a suit", suit)

	suitTyped := Suit(suit)

	log.Printf("Casted %v into suitTyped %v", suit, suitTyped)

	for _, s := range Suits {
		log.Printf("Checking %v if it equals %v", suitTyped, s)
		if suitTyped == s {
			log.Printf("Found a match, returning no errors")
			return suitTyped, nil
		}
	}

	log.Printf("Didn't find a match, returning error")
	return "", fmt.Errorf("%v is not a valid suit", suit)
}

// StringToRank converts a string to a rank or returns an error if invalid
func StringToRank(rank string) (Rank, error) {
	rankTyped := Rank(rank)

	for _, r := range Ranks {
		if rankTyped == r {
			return rankTyped, nil
		}
	}

	return "", fmt.Errorf("%v is not a valid rank", rank)
}

// Card is a combination of a rank and a suit
type Card struct {
	Rank Rank `json:"rank"`
	Suit Suit `json:"suit"`
}

// NewCard creates a new card
func NewCard(rank Rank, suit Suit) *Card {
	return &Card{
		Rank: rank,
		Suit: suit,
	}
}

// Equals checks if the two cards are the same (based on rank and suit)
func (c *Card) Equals(other *Card) bool {
	return c.Rank == other.Rank && c.Suit == other.Suit
}
