// Package models contains the models used through the game logic and it's supporters
package models

import (
	"math/rand"
)

// Deck is a representation of a deck that includes a few
// helper methods that resemble what people would normally
// do with a real deck of cards.
// Top of the deck is index 0
// Bottom of the deck is last index
type Deck struct {
	cards []*Card
}

// NewDeck returns a fresh deck, unshuffled
func NewDeck() *Deck {
	deckCards := make([]*Card, 0)

	for _, suit := range Suits {
		for _, rank := range Ranks {
			deckCards = append(deckCards, NewCard(rank, suit))
		}
	}

	return &Deck{
		cards: deckCards,
	}
}

// NewCustomDeck returns a fresh deck with cards made from the combination of suits and ranks passed
func NewCustomDeck(suits []Suit, ranks []Rank) *Deck {
	deckCards := make([]*Card, 0)

	for _, suit := range suits {
		for _, rank := range ranks {
			deckCards = append(deckCards, NewCard(rank, suit))
		}
	}

	return &Deck{
		cards: deckCards,
	}
}

// Shuffle shuffles the deck
func (d *Deck) Shuffle() {
	for i := range d.cards {
		j := rand.Intn(i + 1)
		d.cards[i], d.cards[j] = d.cards[j], d.cards[i]
	}
}

// Cut will cut the deck in two
func (d *Deck) Cut() {
	cutIndex := rand.Intn(len(d.cards))

	topHalf := d.cards[0:cutIndex]
	botHalf := d.cards[cutIndex:]

	d.cards = append(botHalf, topHalf...)
}

// Deal returns the top 'count' cards of the deck and removes them
// from the Deck's inner storage.
func (d *Deck) Deal(count int) []*Card {
	// Get the cards
	cards := d.cards[0:count]
	// Remove them from the deck
	d.cards = d.cards[count:]

	return cards
}

// AddOnTop adds the cards passed on the top of the deck
func (d *Deck) AddOnTop(cards []*Card) {
	d.cards = append(cards, d.cards...)
}

// AddOnBottom adds the cards passed on the bottom of the deck
func (d *Deck) AddOnBottom(cards []*Card) {
	d.cards = append(d.cards, cards...)
}

// Length returns the length of the deck (how many cards it has)
func (d *Deck) Length() int {
	return len(d.cards)
}
