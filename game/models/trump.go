// Package models contains the models used through the game logic and it's supporters
package models

import "fmt"

// Trump is a card trump
type Trump string

const (
	// AllTrump trump
	AllTrump Trump = "all-trump"
	// NoTrump trump
	NoTrump Trump = "no-trump"
	// SpadesTrump trump
	SpadesTrump Trump = "spades"
	// HeartsTrump trump
	HeartsTrump Trump = "hearts"
	// DiamondsTrump trump
	DiamondsTrump Trump = "diamonds"
	// ClubsTrump trump
	ClubsTrump Trump = "clubs"
	// Pass trump
	Pass Trump = "pass"
)

// TrumpsOrder Lowest index, lowest power
// This array contains all trumps as well
var TrumpsOrder = []Trump{
	Pass,
	ClubsTrump,
	DiamondsTrump,
	HeartsTrump,
	SpadesTrump,
	NoTrump,
	AllTrump,
}

// StringToTrump converts a string (if valid) to a Trump type. If the string is not one of all
// possible Trump values, it will return an error.
func StringToTrump(trump string) (Trump, error) {
	switch trump {
	case "all-trump":
		return AllTrump, nil
	case "no-trump":
		return NoTrump, nil
	case "spades":
		return SpadesTrump, nil
	case "hearts":
		return HeartsTrump, nil
	case "diamonds":
		return DiamondsTrump, nil
	case "clubs":
		return ClubsTrump, nil
	case "pass":
		return Pass, nil
	default:
		return "", fmt.Errorf("%v is not a valid trump", trump)
	}
}

// IsHigherThan compares two trumps. If `t` is higher than `other`
// the function returns true. Otherwise, if equal or lower, the function
// returns false.
func (t Trump) IsHigherThan(other Trump) bool {
	// find index of trump `t`
	tIndex := -1
	for i, trump := range TrumpsOrder {
		if t == trump {
			tIndex = i
			break
		}
	}

	if tIndex == -1 {
		panic(fmt.Sprintf("Could not find trump t '%s'", t))
	}

	// find index of trump `other`
	otherIndex := -1
	for i, trump := range TrumpsOrder {
		if other == trump {
			otherIndex = i
			break
		}
	}

	if otherIndex == -1 {
		panic(fmt.Sprintf("Could not find trump other '%s'\n", other))
	}

	return tIndex > otherIndex
}
