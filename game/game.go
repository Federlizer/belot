// Package game contains the game logic
package game

import (
	"fmt"
	"log"

	"gitlab.com/Federlizer/belot/game/gamecommands"
	"gitlab.com/Federlizer/belot/game/gameevents"
	"gitlab.com/Federlizer/belot/game/models"
	"gitlab.com/Federlizer/belot/protocol"
)

var (
	belotSuits = []models.Suit{models.Spades, models.Clubs, models.Diamonds, models.Hearts}
	belotRanks = []models.Rank{models.Ace, models.Seven, models.Eight, models.Nine, models.Ten, models.Jack, models.Queen, models.King}
)

// Game is the core belot game object that drives the flow of the game, provided 4 players.
// A few key notes to follow on the implementation here. It's important that the helper methods (those that emit events
// to players and so on) don't modify the state of the Game. It should only be the StartGame, setup, play and count
// methods that do that - the actual methods that control the flow of the game.
// Helper methods are only used to communicate with the players and await their actions.
type Game struct {
	// Action is the channel that players can act on
	Action chan *PlayerCommand

	deck    *models.Deck
	owner   *Player
	players []*Player

	dealer       *Player
	currentTrump models.Trump

	boardCards  BoardCards
	teamOnePile []*models.Card
	teamTwoPile []*models.Card
}

// NewGame creates a new game with the specified players
func NewGame(owner *Player, players []*Player) *Game {
	return &Game{
		Action: make(chan *PlayerCommand),

		deck:    models.NewCustomDeck(belotSuits, belotRanks),
		owner:   owner,
		players: players,

		dealer:       owner,
		currentTrump: models.Pass,

		boardCards:  make(BoardCards),
		teamOnePile: make([]*models.Card, 0),
		teamTwoPile: make([]*models.Card, 0),
	}
}

// StartGame is meant to be run as a goroutine.
func (g *Game) StartGame() {
	// Wait for all players to load
	g.awaitPlayersToReady()

	// Notify players of start of game
	for i, player := range g.players {
		playerSouth := g.players[i]
		playerWest := g.players[(i+1)%4]
		playerNorth := g.players[(i+2)%4]
		playerEast := g.players[(i+3)%4]

		payload := map[string]map[string]string{
			"playerSouth": {
				"username": playerSouth.username,
			},
			"playerWest": {
				"username": playerWest.username,
			},
			"playerNorth": {
				"username": playerNorth.username,
			},
			"playerEast": {
				"username": playerEast.username,
			},
		}

		event := protocol.NewEvent(gameevents.GameReadyEvent, payload)

		log.Printf("Sending game ready event with payload %v", payload)
		player.emit <- event
	}

	log.Printf("Sent game-ready event to all players")

	// Begin a game
	log.Println("Game of Belot has started.")
	log.Printf("%v is owner of the game.", g.owner.username)

	g.deck.Shuffle()

	// Game loop
	// Dealer is owner at the beginning of the game (at game creation)
	for {
		if g.deck.Length() != 32 {
			panic("Deck isn't exactly 32 cards!")
		}

		// Stage one - Setup (deal cards, choose trump or pass)
		pass := g.setup()

		if pass {
			// Let players know that the game was passed
			// TODO: Make this better - send passed event to client, handle that appropriately on the client as well
			g.count()
			g.awaitPlayersToReady()

			// Take away player cards
			playerCards := make([]*models.Card, 0)
			playerCards = append(playerCards, g.players[0].TakeAwayCards()...)
			playerCards = append(playerCards, g.players[1].TakeAwayCards()...)
			playerCards = append(playerCards, g.players[2].TakeAwayCards()...)
			playerCards = append(playerCards, g.players[3].TakeAwayCards()...)

			// Add them back to the deck
			g.deck.AddOnBottom(playerCards)
			g.deck.Cut()

			// Assign new dealer
			g.dealer = g.playerWestOf(g.dealer)

			continue
		}

		// Stage two - Play
		g.play()

		// Stage three - Count
		g.count()

		// Wait for all players to be ready again (after viewing scores)
		g.awaitPlayersToReady()

		// Stage four - Prep next round

		// Combine deck and cut at a random point
		g.deck.AddOnTop(g.teamOnePile)
		g.deck.AddOnTop(g.teamTwoPile)
		g.deck.Cut()

		// Clean board and empty team piles
		g.boardCards = make(BoardCards)
		g.teamOnePile = make([]*models.Card, 0)
		g.teamTwoPile = make([]*models.Card, 0)

		// Assign new dealer
		g.dealer = g.playerWestOf(g.dealer)
	}
	/*
		// Repeat until one team has gotten 151 points
		gameOverEvent := protocol.NewEvent(gameevents.GameOverEvent, nil)
		g.emitToAll(gameOverEvent)

		log.Println("Game of Belot has ended.")
	*/
}

// setup sets up the game before starting the actual card playing
// includes: dealing first 5 cards, chosing a trump, dealing rest of cards

// Will return true or false based on if the outcome of the setup was a pass (i.e. skip play, go to next)
func (g *Game) setup() (pass bool) {
	log.Println("Setup game")

	// Deal first three
	g.playerWestOf(g.dealer).GiveCards(g.deck.Deal(3))
	g.playerNorthOf(g.dealer).GiveCards(g.deck.Deal(3))
	g.playerEastOf(g.dealer).GiveCards(g.deck.Deal(3))
	g.dealer.GiveCards(g.deck.Deal(3))

	// Deal two more
	g.playerWestOf(g.dealer).GiveCards(g.deck.Deal(2))
	g.playerNorthOf(g.dealer).GiveCards(g.deck.Deal(2))
	g.playerEastOf(g.dealer).GiveCards(g.deck.Deal(2))
	g.dealer.GiveCards(g.deck.Deal(2))

	// Notify players that the cards were dealt
	g.emitGameboardToAll()

	// Await players to chose a trump
	g.requestAction(g.dealer, gamecommands.SelectTrumpCommand)
	selectedTrump := g.awaitTrumpSelect()

	// Go to next in case of pass
	if selectedTrump == models.Pass {
		return true
	}

	g.currentTrump = selectedTrump

	// Deal last three
	g.playerWestOf(g.dealer).GiveCards(g.deck.Deal(3))
	g.playerNorthOf(g.dealer).GiveCards(g.deck.Deal(3))
	g.playerEastOf(g.dealer).GiveCards(g.deck.Deal(3))
	g.dealer.GiveCards(g.deck.Deal(3))

	if g.deck.Length() != 0 {
		panic("Not an empty deck after beginning of game!!!!")
	}

	// Let players know that the cards were dealt out
	g.emitGameboardToAll()

	log.Println("Setup is done")
	return false
}

// play takes control of an already setup game and implements the standard game loop of belot game
//
// A couple of assumptions are made by this function:
// 1. The players are positioned in a standard belot game fashion (teammates are sitting opposite eachother)
// 2. The player pairs are index 0 and 2 in one team, index 1 and 3 in the other
// 3. The players have been dealt all cards and have decided on a suit
// 4. The player that starts sits on the left of the dealer
// 5. The g.boardCards is already clean
func (g *Game) play() {
	log.Println("Play game")

	// First is left of dealer
	underhandPlayer := g.playerWestOf(g.dealer)
	currentPlayer := underhandPlayer

	// Hand loop (each time underhandPlayer changes)
	for len(underhandPlayer.hand) > 0 {

		// Playing cards loop (each time a player plays a hand inside a single hand loop)
		for {
			log.Printf("Player '%v' should play a card", currentPlayer.username)
			g.requestAction(currentPlayer, gamecommands.PlayCardCommand)

			cardPlayed := currentPlayer.PlayCard(g.Action)
			log.Printf("Player '%v' played card %v", currentPlayer.username, cardPlayed)

			// Assert boardCards state is clean for this player
			if _, exists := g.boardCards[currentPlayer]; exists {
				panic("Unclean board or player playing twice!!!")
			}

			// Update players on board state
			g.boardCards[currentPlayer] = cardPlayed
			g.emitGameboardToAll()

			if g.playerWestOf(currentPlayer) == underhandPlayer {
				log.Printf("All players have played a card this hand loop, now it's time to choose who won the hand...")
				break
			}

			// Otherwise continue current hand
			currentPlayer = g.playerWestOf(currentPlayer)
		}

		// Dealer decides who won the hand
		// Request action from dealer
		g.requestAction(g.dealer, gamecommands.HandWinnerCommand)
		var handWinner *Player
		for {
			username := g.dealer.DecideHandWinner(g.Action)

			handWinner = g.findPlayerByUsername(username)
			if handWinner == nil {
				// Username wasn't found
				g.dealer.emit <- protocol.NewErrorEvent("Player with that username couldn't be found")
				continue
			}

			break
		}

		// Save the hand in team's pile
		handWinnerIndex := g.findPlayerIndexFor(handWinner)
		switch handWinnerIndex {
		case 0:
			fallthrough
		case 2:
			log.Printf("Adding g.boardCards.cards() to g.teamOnePile: %v", g.boardCards.cards())
			g.teamOnePile = append(g.teamOnePile, g.boardCards.cards()...)

		case 1:
			fallthrough
		case 3:
			log.Printf("Adding g.boardCards.cards() to g.teamTwoPile: %v", g.boardCards.cards())
			g.teamTwoPile = append(g.teamTwoPile, g.boardCards.cards()...)

		default:
			panic("Got unknown index for hand winner")
		}

		// Setup for next hand
		underhandPlayer = handWinner
		currentPlayer = handWinner

		g.boardCards.clear()
		g.emitGameboardToAll()
	}

	log.Println("Playing is done")
}

func (g *Game) count() {
	log.Println("Count points")

	payload := map[string][]*models.Card{
		fmt.Sprintf("Team %v and %v", g.players[0].username, g.players[2].username): g.teamOnePile,
		fmt.Sprintf("Team %v and %v", g.players[1].username, g.players[3].username): g.teamTwoPile,
	}

	// Let players know the round is done
	roundDoneEvent := protocol.NewEvent(gameevents.RoundDoneEvent, payload)
	g.emitToAll(roundDoneEvent)

	log.Println("Counting done")
}

// awaitPlayersToReady will wait until all players have been able to send a ready command
func (g *Game) awaitPlayersToReady() {
	var (
		playerOneReady   = false
		playerTwoReady   = false
		playerThreeReady = false
		playerFourReady  = false
	)

	for !playerOneReady || !playerTwoReady || !playerThreeReady || !playerFourReady {
		log.Printf("Not all players are ready, awaiting new command...")
		playerCmd := <-g.Action
		log.Printf("Got new command while waiting for players to load: %v", playerCmd)

		if playerCmd.Command.Command != gamecommands.ReadyCommand {
			log.Printf("Not a %v command, skipping", gamecommands.ReadyCommand)
			playerCmd.Player.emit <- protocol.NewErrorEvent("All players must be ready to play first")
			continue
		}

		switch {
		case playerCmd.Player == g.players[0]:
			log.Printf("PlayerOne (%v) has declared they are ready", g.players[0])
			playerOneReady = true

		case playerCmd.Player == g.players[1]:
			log.Printf("PlayerTwo (%v) has declared they are ready", g.players[1])
			playerTwoReady = true

		case playerCmd.Player == g.players[2]:
			log.Printf("PlayerThree (%v) has declared they are ready", g.players[2])
			playerThreeReady = true

		case playerCmd.Player == g.players[3]:
			log.Printf("PlayerFour (%v) has declared they are ready", g.players[3])
			playerFourReady = true

		default:
			log.Printf("A player that is NOT supposed to be in the game has sent a ready command. Player command: %v", playerCmd)
		}

		log.Printf("One: %v, two: %v, three: %v, four: %v", playerOneReady, playerTwoReady, playerThreeReady, playerFourReady)
	}

	log.Printf("All players are ready, begin!")
}

// calculateGameboardForPlayer calculates the gameboard event object that needs to be sent to an individual player.
// Each player cannot see other player's hands, so we  need to calculate it for each player individually.
func (g *Game) calculateGameboardForPlayer(playerIndex int) *Gameboard {
	gameboard := &Gameboard{
		Hands:      make(map[string]*GameboardHand),
		BoardCards: make(map[string]*models.Card),
	}

	playerSouthIndex := playerIndex
	playerWestIndex := (playerIndex + 1) % 4
	playerNorthIndex := (playerIndex + 2) % 4
	playerEastIndex := (playerIndex + 3) % 4

	playerSouth := g.players[playerSouthIndex]
	gameboard.BoardCards["playerSouth"] = g.boardCards[playerSouth]
	gameboard.Hands["playerSouth"] = &GameboardHand{
		Hand:       playerSouth.hand,
		HandLength: len(playerSouth.hand),
	}

	playerWest := g.players[playerWestIndex]
	gameboard.BoardCards["playerWest"] = g.boardCards[playerWest]
	gameboard.Hands["playerWest"] = &GameboardHand{
		Hand:       make([]*models.Card, 0), // Don't show the hands to other players
		HandLength: len(playerWest.hand),
	}

	playerNorth := g.players[playerNorthIndex]
	gameboard.BoardCards["playerNorth"] = g.boardCards[playerNorth]
	gameboard.Hands["playerNorth"] = &GameboardHand{
		Hand:       make([]*models.Card, 0), // Don't show the hands to other players
		HandLength: len(playerNorth.hand),
	}

	playerEast := g.players[playerEastIndex]
	gameboard.BoardCards["playerEast"] = g.boardCards[playerEast]
	gameboard.Hands["playerEast"] = &GameboardHand{
		Hand:       make([]*models.Card, 0), // Don't show the hands to other players
		HandLength: len(playerEast.hand),
	}

	return gameboard
}

// requestAction emits an ActionRequest (through an Event) to a player
func (g *Game) requestAction(player *Player, commandName string) {
	payload := map[string]string{
		"actionRequested": commandName,
		"player":          player.username,
	}

	actionRequestEvent := protocol.NewEvent(gameevents.ActionRequestEvent, payload)

	for _, player := range g.players {
		player.emit <- actionRequestEvent
	}
}

func (g *Game) awaitTrumpSelect() models.Trump {
	for {
		playerCmd := <-g.Action
		log.Printf("Received command while waiting for trump: %v", playerCmd)

		// Make sure player who decides the trump is the owner of the game
		if playerCmd.Player != g.dealer {
			log.Printf("%v is not owner of the lobby, skipping", playerCmd.Player.username)
			playerCmd.Player.emit <- protocol.NewErrorEvent("Not your turn")
			continue
		}

		// Is it a trump selected command?
		if playerCmd.Command.Command != gamecommands.SelectTrumpCommand {
			playerCmd.Player.emit <- protocol.NewErrorEvent("You need to select a trump")
			continue
		}

		payload, err := gamecommands.ValidateAndParseSelectedTrumpPayload(playerCmd.Command)
		if err != nil {
			playerCmd.Player.emit <- protocol.NewErrorEvent(err.Error())
			continue
		}

		return payload.Trump
	}
}

/*
// Close closes all channels related to the Game object (including its own Exec channel)
func (g *Game) Close() {
	// TODO maybe we should wait for the exec channel to be closed?
	// or just rethink when to close those channels, because this object (Game)
	// shouldn't really close its own exec channel, as others might still be sending to it
	for _, player := range g.players {
		close(player.emit)
	}

	close(g.Exec)
}
*/
