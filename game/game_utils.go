// Package game contains the game logic
package game

import (
	"gitlab.com/Federlizer/belot/game/gameevents"
	"gitlab.com/Federlizer/belot/protocol"
)

// westOf returns the index of a player that is sitting to the west of player index passed.
// Only works in BELOT games
func westOf(i int) int {
	return (i + 1) % 4
}

// northOf returns the index of a player that is sitting to the north of player index passed.
// Only works in BELOT games
func northOf(i int) int {
	return (i + 2) % 4
}

// eastOf returns the index of a player that is sitting to the east of player index passed.
// Only works in BELOT games
func eastOf(i int) int {
	return (i + 3) % 4
}

// emitToAll emits an event to all players
func (g *Game) emitToAll(event *protocol.Event) {
	for _, player := range g.players {
		player.emit <- event
	}
}

func (g *Game) emitGameboardToAll() {
	for i, player := range g.players {
		gameboard := g.calculateGameboardForPlayer(i)
		player.emit <- protocol.NewEvent(gameevents.UpdateGameboardEvent, gameboard)
	}
}

// findPlayerIndexFor tries to find the player index for the passed player.
// NOTE: This function will panic if a player couldn't be found
func (g *Game) findPlayerIndexFor(player *Player) int {
	for i, p := range g.players {
		if p == player {
			return i
		}
	}

	// Couldn't find in-game player that matches `player`
	panic("Couldn't find in-game player that matches")
}

// findPlayerByUsername tries to find a player in the game with the passed username. If nothing is found, nil is returned.
func (g *Game) findPlayerByUsername(username string) *Player {
	for _, p := range g.players {
		if p.username == username {
			return p
		}
	}

	// Nothing was found
	return nil
}

// playerWestOf returns the player sitting west of the player passed
func (g *Game) playerWestOf(player *Player) *Player {
	playerIndex := g.findPlayerIndexFor(player)
	playerWestIndex := westOf(playerIndex)

	return g.players[playerWestIndex]
}

// playerNorthOf returns the player sitting north of the player passed
func (g *Game) playerNorthOf(player *Player) *Player {
	playerIndex := g.findPlayerIndexFor(player)
	playerNorthIndex := northOf(playerIndex)

	return g.players[playerNorthIndex]
}

// playerEastOf returns the player sitting east of the player passed
func (g *Game) playerEastOf(player *Player) *Player {
	playerIndex := g.findPlayerIndexFor(player)
	playerEastIndex := eastOf(playerIndex)

	return g.players[playerEastIndex]
}
