// Package game contains the game logic
package game

import "gitlab.com/Federlizer/belot/protocol"

// PlayerCommand is a wrapper around Command that also has info about which player executed the command
type PlayerCommand struct {
	Command *protocol.Command
	Player  *Player
}

// NewPlayerCommand creates a new PlayerCommand struct
func NewPlayerCommand(command *protocol.Command, player *Player) *PlayerCommand {
	return &PlayerCommand{
		Command: command,
		Player:  player,
	}
}
