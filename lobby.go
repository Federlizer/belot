// Package main is the main package... duh
package main

import (
	"errors"
	"math/rand"
	"time"

	"gitlab.com/Federlizer/belot/commands"
	"gitlab.com/Federlizer/belot/game"
	"gitlab.com/Federlizer/belot/protocol"
)

const (
	lobbyCodeCharset = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	lobbyCodeLength  = 6
)

var seededRand = rand.New(rand.NewSource(time.Now().UnixNano()))

func generateLobbyCode() string {
	b := make([]byte, lobbyCodeLength)
	for i := range b {
		b[i] = lobbyCodeCharset[seededRand.Intn(len(lobbyCodeCharset))]
	}

	return string(b)
}

// Lobby is a lobby that clients can join and start a game from
type Lobby struct {
	Owner        *Client   `json:"owner"`
	Code         string    `json:"code"`
	Participants []*Client `json:"participants"`

	game *game.Game
}

// NewLobby creates a new lobby and assigns the owner
func NewLobby(owner *Client) *Lobby {
	return &Lobby{
		Owner:        owner,
		Code:         generateLobbyCode(),
		Participants: make([]*Client, 0),
	}
}

// Join adds a client into the lobby.
func (l *Lobby) Join(client *Client) error {
	// TODO: Should use a mutex, so only one client can join at a time.
	if len(l.Participants) >= 4 {
		return errors.New("Lobby is full")
	}

	l.Participants = append(l.Participants, client)

	// Notify other participants of the change
	for _, participant := range l.Participants {
		if participant.Username == client.Username {
			continue
		}
		participant.emit <- protocol.NewEvent(commands.LobbyUpdatedEvent, l)
	}

	return nil
}

// StartGame starts the game
func (l *Lobby) StartGame() error {
	// Prepare players
	if len(l.Participants) < 4 {
		return errors.New("Not enough players")
	}

	var owner *game.Player
	players := make([]*game.Player, 0)

	for _, participant := range l.Participants {
		player := game.NewPlayer(participant.Username, participant.emit)
		participant.player = player

		if l.Owner == participant { // declare owner also
			owner = player
		}

		players = append(players, player)
	}

	// Create game object
	game := game.NewGame(owner, players)
	l.game = game

	// Start game (separate goroutine)
	go l.game.StartGame()

	// Notify clients that game has started
	event := protocol.NewEvent(commands.GameStartedEvent, nil)
	for _, participant := range l.Participants {
		participant.emit <- event
	}

	return nil
}
