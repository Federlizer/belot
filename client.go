// Package main is the main package... duh
package main

import (
	"log"

	"github.com/gorilla/websocket"
	"gitlab.com/Federlizer/belot/commands"
	"gitlab.com/Federlizer/belot/game"
	"gitlab.com/Federlizer/belot/protocol"
)

// defaultUsername is the default username that is assigned to clients before they get registered.
// It also means that the default username cannot be used as a username when registering
const defaultUsername = ""

// Client struct represents a websocket client
type Client struct {
	emit      chan *protocol.Event
	conn      *websocket.Conn
	closeChan chan bool

	Username string `json:"username"`

	hub    *Hub
	lobby  *Lobby
	player *game.Player
}

// NewClient returns a pointer to a new Client
func NewClient(conn *websocket.Conn, hub *Hub) *Client {
	client := &Client{
		emit: make(chan *protocol.Event),
		// Commands: make(chan *protocol.Command),

		conn:      conn,
		closeChan: make(chan bool),

		Username: defaultUsername,
		hub:      hub,
		lobby:    nil,
	}

	return client
}

// writePump is a long-running goroutine that writes the events sent to the `Emit` channel
func (c *Client) writePump() {
	defer func() {
		// TODO: Try to use something more ambiguous, like the IP?
		log.Printf("writePump: Closing connection for %v", c)
		if err := c.conn.Close(); err != nil {
			log.Printf("Error while closing WS connection: %v", err)
		}
	}()

	for {
		select {
		case event := <-c.emit:
			log.Printf("Sending event to client %v", event) // Transform to JSON and handle error
			b, err := event.ToJSON()
			if err != nil {
				log.Fatalf("error while parsing event to JSON: %v", err)
				break //NOTE: maybe we don't even need this?
			}

			if err := c.conn.WriteMessage(websocket.TextMessage, b); err != nil {
				log.Fatalf("error while sending message to ws client: %v", err)
				break //NOTE: maybe we don't even need this?
			}
		case <-c.closeChan:
			log.Printf("Going to have to close this goroutine")
			return
		}
	}
}

// readPump is a long-running goroutine that receives messages from the websocket client and
// passes them down to the `Commands` channel
func (c *Client) readPump() {
	defer func() {
		// TODO: Try to use something more ambiguous, like the IP?
		log.Printf("readPump: Closing connection for %v", c)
		if err := c.conn.Close(); err != nil {
			log.Printf("Error while closing WS connection: %v", err)
		}
		// close(c.Commands)
		c.closeChan <- true
	}()

	for {
		// Receive message
		_, message, err := c.conn.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseNormalClosure, websocket.CloseAbnormalClosure, websocket.CloseGoingAway) {
				log.Fatalf("error: %v", err)
			}
			break
		}

		// Transform message into command
		command, err := protocol.NewCommandFromJSON(message)
		if err != nil {
			c.emit <- protocol.NewErrorEvent(err.Error())
			continue
		}

		event := c.processCommand(command)
		if event == nil {
			log.Println("Skipping empty event, probably handled by something else (lobby start game or game itself?)")
			continue
		}

		c.emit <- event
	}
}

// processCommand processes a command passed by the client
func (c *Client) processCommand(command *protocol.Command) *protocol.Event {
	log.Printf("Processing command %v for client %v", command, c)

	switch command.Command {
	case commands.RegisterCommand:
		log.Printf("Client wants to be registered with command %v", command)

		if c.Username != defaultUsername {
			return protocol.NewErrorEvent("Already registered")
		}

		payload, err := commands.ValidateAndParseRegisterCommandPayload(command)
		if err != nil {
			return protocol.NewErrorEvent(err.Error())
		}

		// Ensure username passed is not defaultUsername
		if payload.Username == defaultUsername {
			return protocol.NewErrorEvent("Cannot user this username")
		}

		err = c.hub.RegisterClient(payload.Username, c)
		if err != nil {
			return protocol.NewErrorEvent(err.Error())
		}

		c.Username = payload.Username
		return protocol.NewEvent(commands.RegisteredEvent, nil)

	case commands.CreateLobbyCommand:
		log.Println("Client wants to create a lobby")

		// They need to register first
		if c.Username == defaultUsername {
			return protocol.NewErrorEvent("You need to regsiter first")
		}

		// They cannot be in another lobby
		if c.lobby != nil {
			return protocol.NewErrorEvent("You need to exit current lobby first")
		}

		// Create a new lobby
		lobby := NewLobby(c)
		lobby.Join(c)

		// Ensure client has access to lobby as well
		c.lobby = lobby

		// Add lobby to hub
		c.hub.AddLobby(lobby)

		event := protocol.NewEvent(commands.LobbyCreatedEvent, lobby)
		return event

	case commands.JoinLobbyCommand:
		log.Printf("Client wants to join lobby with command %v", command)

		// They need to register first
		if c.Username == defaultUsername {
			return protocol.NewErrorEvent("You need to register first")
		}

		// They need to exit current lobby first
		if c.lobby != nil {
			return protocol.NewErrorEvent("You need to exit current lobby first")
		}

		payload, err := commands.ValidateAndParseJoinLobbyCommandPayload(command)
		if err != nil {
			return protocol.NewErrorEvent(err.Error())
		}

		lobby := c.hub.FindLobbyByCode(payload.Code)
		if lobby == nil {
			return protocol.NewErrorEvent("A lobby with that code cannot be found")
		}

		// Join
		err = lobby.Join(c)
		if err != nil {
			return protocol.NewErrorEvent(err.Error())
		}

		c.lobby = lobby

		event := protocol.NewEvent(commands.LobbyJoinedEvent, lobby)
		return event

	case commands.StartGameCommand:
		log.Printf("Client wants to start the game with command %v", command)

		err := c.lobby.StartGame()
		if err != nil {
			return protocol.NewErrorEvent(err.Error())
		}

		// No need to send event, lobby will do that
		return nil

	case commands.GameCommand:
		log.Printf("Client wants to send a game command to the game: %v", command)

		// Game hasn't started yet
		if c.lobby.game == nil {
			return protocol.NewErrorEvent("Game hasn't started yet!")
		}

		// Make sure we have the player instance also
		if c.player == nil {
			log.Fatalf("Game %v has started, but client %v doesn't have a player", c.lobby.game, c)
			panic("In case above didn't close the program")
		}

		gameCommandPayload, err := commands.ValidateAndParseGameCommandPayload(command)
		if err != nil {
			return protocol.NewErrorEvent(err.Error())
		}

		log.Printf("Sending gameCommandPayload %v to game", gameCommandPayload)
		playerCommand := game.NewPlayerCommand(gameCommandPayload.Command, c.player)
		c.lobby.game.Action <- playerCommand

		return nil

	default:
		return protocol.NewErrorEvent("Unknown command")
	}
}
