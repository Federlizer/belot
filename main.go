// Package main is the main package... duh
package main

import (
	"log"
	"net/http"

	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	// TODO: Get rid of this or make it better
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

func main() {
	// Configure logger
	log.SetFlags(log.Ldate | log.Ltime | log.Lshortfile | log.LUTC)

	// Configure hub
	hub := NewHub()

	// Setup http handlers
	SetupHandlers(hub)

	// Start http server
	log.Println("Starting server")
	log.Fatal(http.ListenAndServe(":8080", nil))
}

// SetupHandlers setups the handlers for an http server
func SetupHandlers(hub *Hub) {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		log.Println("Sending index.html")
		http.ServeFile(w, r, "index.html")
	})

	http.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		conn, err := upgrader.Upgrade(w, r, nil)
		if err != nil {
			log.Printf("Received an error while upgrading client: %v", err)
			return
		}

		client := NewClient(conn, hub)

		// Start PUMPIIIIING
		go client.writePump()
		go client.readPump()
	})
}
