// Package main is the main package... duh
package main

import (
	"errors"
	"log"
	"sync"
)

// Hub is the main place clients and lobbies are stored
type Hub struct {
	lobbies    []*Lobby
	playerRepo sync.Map
}

// NewHub creates a new hub
func NewHub() *Hub {
	return &Hub{
		lobbies:    make([]*Lobby, 0),
		playerRepo: sync.Map{},
	}
}

// RegisterClient function registers a new client as a player.
func (h *Hub) RegisterClient(username string, client *Client) error {
	_, loaded := h.playerRepo.LoadOrStore(username, client)
	if loaded {
		// Player username already in  use
		return errors.New("Username already in use")
	}

	log.Printf("Registered client with username %v", username)
	return nil
}

// AddLobby adds a lobby to the lobby list, available for others to join
func (h *Hub) AddLobby(lobby *Lobby) {
	// TODO: Ensure only one lobby with a code exists
	// TODO: Only allow one insertion at a time (mutex?)
	// TODO: Take away control from Client to create lobbies themselves? Not essential though

	h.lobbies = append(h.lobbies, lobby)
}

// FindLobbyByCode attempts to find a lobby with the passed code. Returns a nil pointer
// if nothing is found
func (h *Hub) FindLobbyByCode(code string) *Lobby {
	var foundLobby *Lobby
	for _, lobby := range h.lobbies {
		if lobby.Code == code {
			foundLobby = lobby
		}
	}

	return foundLobby
}
