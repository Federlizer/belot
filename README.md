# Belot game server written in Go

A game server written in go that allows clients to play belot. Uses websockets
to communicate with the clients.


### Next steps

* Belot Server
  * Make sure that you cannot execute commands when you haven't registered. How can we ensure that that is always the case?
  * Implement deck shufling
  * Implement game flow
    1. [DONE] Deal out cards
    2. Players must decide trump or pass the round (pass means go to 1)
    3. After players decide trump, deal out the rest of the cards
    4. (Dealer)/(The last who took) plays a card
    5. The rest of the players take turns to play a card
    6. "Dealer" decides who takes the hand
    7. Go to 4

* Belot Client
  * Ensure user is logged in, otherwise redirect them to the login page


### Tests/Break the system

* Ensure usernames are cleared when users close WS connections
* Update Lobby.Join method to use a mutex so only one client can join at a time (prevent race conditions)
* Find a way to clean up lobbies that have been left empty
* Can you confuse the Command parser (payload/Command.NewCommandFromJSON) to receive a 'client' key?
* Is it a problem that the auto generated codes aren't forced to be unique? (probably yes, but highly unlikely to get a duplicate code)
