// Package protocol implements the components that are used to communicate between
// the websocket server and its clients
package protocol

import (
	"encoding/json"
	"errors"
)

// Command struct is a command sent by a client and to be processed by the websocket server
type Command struct {
	Command string                 `json:"command"`
	Payload map[string]interface{} `json:"payload"`
}

// NewCommand creates a new command
func NewCommand(command string, payload map[string]interface{}) *Command {
	return &Command{
		Command: command,
		Payload: payload,
	}
}

// NewCommandFromJSON attempts to parse `b`
func NewCommandFromJSON(b []byte) (*Command, error) {
	// Validate JSON
	valid := json.Valid(b)
	if !valid {
		return nil, errors.New("Invalid JSON")
	}

	// Genereate command
	command := &Command{}
	err := json.Unmarshal(b, command)

	if err != nil {
		return nil, err
	}

	// TODO: Validate command struct?
	// Command and Payload should be present, but client not!

	return command, nil
}
