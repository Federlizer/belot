// Package protocol implements the components that are used to communicate between
// the websocket server and its clients
package protocol

import "encoding/json"

const (
	// ErrorEvent is an event that signifies an error
	ErrorEvent = "error"
)

// Event struct is an event emitted to one or more clients
type Event struct {
	Event string      `json:"event"`
	Value interface{} `json:"value"`
}

// NewEvent creates a new event
func NewEvent(event string, value interface{}) *Event {
	return &Event{
		Event: event,
		Value: value,
	}
}

// NewErrorEvent creates a new event that signifies an error (i.e. it has Event set to `error`)
func NewErrorEvent(reason string) *Event {
	return NewEvent("error", map[string]string{
		"reason": reason,
	})
}

func (e *Event) ToJSON() ([]byte, error) {
	return json.Marshal(e)
}
