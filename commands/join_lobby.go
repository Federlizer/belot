// Package commands implements the payloads of different available commands
// and their validators and parsers
package commands

import (
	"errors"
	"fmt"
	"reflect"

	"gitlab.com/Federlizer/belot/protocol"
)

// JoinLobbyCommand is a command issued when a registered player wants to join a lobby in the hub
const JoinLobbyCommand = "join-lobby"

// LobbyJoinedEvent is an event that signifies a successful joining of a lobby
const LobbyJoinedEvent = "lobby-joined"

// LobbyUpdatedEvent is an event that signifies that the lobby has been updated in some way
// (right now, only that a new participant has joined the lobby)
const LobbyUpdatedEvent = "lobby-updated"

// JoinLobbyPayload is the payload requested for the join-lobby command to work properly
type JoinLobbyPayload struct {
	Code string
}

// ValidateJoinLobbyCommandPayload validates the payload of a join-lobby command. Will panic if the command
// name doesn't equal `JoinLobbyCommand`
func ValidateJoinLobbyCommandPayload(command *protocol.Command) error {
	if command.Command != JoinLobbyCommand {
		panic(fmt.Sprintf("not a %v command", JoinLobbyCommand))
	}

	codeInterface, ok := command.Payload["code"]
	if !ok {
		return errors.New("'code' key must be present for this command")
	}

	codeType := reflect.TypeOf(codeInterface)
	if codeType.Kind() != reflect.String {
		return errors.New("'code' key must be a string")
	}

	return nil
}

// ParseJoinLobbyCommandPayload attempts to parse the payload of a join-lobby command. It will NOT validate
// the payload first, so remember to use `ValidateJoinLobbyCommandPayload` beforehand or the function
// might panic or return trash output.
func ParseJoinLobbyCommandPayload(command *protocol.Command) *JoinLobbyPayload {
	codeInterface := command.Payload["code"]
	codeValue := reflect.ValueOf(codeInterface)
	code := codeValue.String()

	return &JoinLobbyPayload{
		Code: code,
	}
}

// ValidateAndParseJoinLobbyCommandPayload first validates the payload then parses it and returns the parsed
// payload. If there are any validation errors, the RegisterPayload value will be nil.
func ValidateAndParseJoinLobbyCommandPayload(command *protocol.Command) (*JoinLobbyPayload, error) {
	err := ValidateJoinLobbyCommandPayload(command)
	if err != nil {
		return nil, err
	}

	payload := ParseJoinLobbyCommandPayload(command)
	return payload, err
}
