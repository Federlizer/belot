// Package commands implements the payloads of different available commands
// and their validators and parsers
package commands

// StartGameCommand is a command issued when a client wants to start a game
const StartGameCommand = "start-game"

// GameStartedEvent is an event that signifies that a game has started
const GameStartedEvent = "game-started"
