// Package commands implements the payloads of different available commands
// and their validators and parsers
package commands

// CreateLobbyCommand is a command issued when a registered player wants to create a new lobby in the Hub
const CreateLobbyCommand = "create-lobby"

// LobbyCreatedEvent is an event that signifies a successful creation of a lobby
const LobbyCreatedEvent = "lobby-created"
