// Package commands implements the payloads of different available commands
// and their validators and parsers
package commands

import (
	"errors"
	"fmt"
	"log"
	"reflect"

	"gitlab.com/Federlizer/belot/protocol"
)

// GameCommand is a command that should get passed down to the active game
const GameCommand = "game-command"

// GameCommandPayload is the payload required for the GameCommmand to work properly
type GameCommandPayload struct {
	Command *protocol.Command
}

// ValidateGameCommandPayload validates the payload of a game command. Will panic if the command
// name doesn't equal `GameCommand`
func ValidateGameCommandPayload(command *protocol.Command) error {
	// Validate command name
	if command.Command != GameCommand {
		panic(fmt.Sprintf("not a %v command", GameCommand))
	}

	// Validate payload has 'command' key as a string
	innerCommandInterface, ok := command.Payload["command"]
	if !ok {
		return errors.New("'command' key must be present inside payload")
	}

	innerCommandType := reflect.TypeOf(innerCommandInterface)
	if innerCommandType.Kind() != reflect.String {
		return errors.New("'command' key inside payload must be of type string")
	}

	// Validate payload has 'payload' key as an object
	innerPayloadInterface, ok := command.Payload["payload"]
	if !ok {
		return errors.New("'payload' key must be present inside payload")
	}

	innerPayloadType := reflect.TypeOf(innerPayloadInterface)
	if innerPayloadType == nil || innerPayloadType.Kind() != reflect.Map {
		return errors.New("'payload' key inside payload must be of type object/map")
	}

	return nil
}

// ParseGameCommandPayload attempts to parse the payload of a game command. It will NOT validate
// the payload first, so remember to use `ValidateGameCommandPayload` beforehand or the function
// might panic or return trash output.
func ParseGameCommandPayload(command *protocol.Command) *GameCommandPayload {
	innerCommandInterface := command.Payload["command"]
	innerCommandValue := reflect.ValueOf(innerCommandInterface)
	innerCommand := innerCommandValue.String()

	log.Printf("Parsed innerCommand: %v", innerCommand)

	innerPayloadInterface := command.Payload["payload"]
	innerPayloadValue := reflect.ValueOf(innerPayloadInterface)
	innerPayload := make(map[string]interface{})

	log.Printf("Starting mapRange")
	iter := innerPayloadValue.MapRange()
	for iter.Next() {
		log.Printf("Iter.Next()")
		key := iter.Key().String()
		log.Printf("Key: %v", key)
		value := iter.Value().Interface()
		log.Printf("Value: %v", value)

		innerPayload[key] = value
		log.Printf("Updated innerPayload: %v", innerPayload)
	}

	log.Printf("Completed mapRange. Full innerPayload: %v", innerPayload)

	return &GameCommandPayload{
		Command: protocol.NewCommand(innerCommand, innerPayload),
	}
}

// ValidateAndParseGameCommandPayload first validates the payload then parses it and returns the parsed
// payload. If there are any validation errors, the GameCommandPayload value will be nil.
func ValidateAndParseGameCommandPayload(command *protocol.Command) (*GameCommandPayload, error) {
	err := ValidateGameCommandPayload(command)
	if err != nil {
		return nil, err
	}

	log.Printf("Validated command %v", command)

	payload := ParseGameCommandPayload(command)
	log.Printf("Parsed command %v that turned out as payload %v", command, payload)
	return payload, nil
}
