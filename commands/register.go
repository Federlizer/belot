// Package commands implements the payloads of different available commands
// and their validators and parsers
package commands

import (
	"errors"
	"fmt"
	"reflect"

	"gitlab.com/Federlizer/belot/protocol"
)

// RegisterCommand is a command issued when a client wants to be registered as a player in the Hub.
const RegisterCommand = "register"

// RegisteredEvent is an event that signifies a successful register
const RegisteredEvent = "registered"

// RegisterPayload is the payload requested for the register command to work properly
type RegisterPayload struct {
	Username string
}

// ValidateRegisterCommandPayload validates the payload of a register command. Will panic if the command
// name doesn't equal `RegisterCommand`
func ValidateRegisterCommandPayload(command *protocol.Command) error {
	// Validate command name
	if command.Command != RegisterCommand {
		panic(fmt.Sprintf("not a %v command", RegisterCommand))
	}

	// Validate command has username as a string
	usernameInterface, ok := command.Payload["username"]
	if !ok { // username wasn't passed
		return errors.New("'username' key must be present for this command")
	}

	usernameType := reflect.TypeOf(usernameInterface)
	if usernameType.Kind() != reflect.String {
		return errors.New("'username' key must be of type string")
	}

	return nil
}

// ParseRegisterCommandPayload attempts to parse the payload of a register command. It will NOT validate
// the payload first, so remember to use `ValidateRegisterCommandPayload` beforehand or the function
// might panic or return trash output.
func ParseRegisterCommandPayload(command *protocol.Command) *RegisterPayload {
	usernameInterface := command.Payload["username"]
	usernameValue := reflect.ValueOf(usernameInterface)
	username := usernameValue.String()

	return &RegisterPayload{
		Username: username,
	}
}

// ValidateAndParseRegisterCommandPayload first validates the payload then parses it and returns the parsed
// payload. If there are any validation errors, the RegisterPayload value will be nil.
func ValidateAndParseRegisterCommandPayload(command *protocol.Command) (*RegisterPayload, error) {
	err := ValidateRegisterCommandPayload(command)
	if err != nil {
		return nil, err
	}

	payload := ParseRegisterCommandPayload(command)
	return payload, err
}
